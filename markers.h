/* ChronoCal
*/

#ifndef MARKER_H
#define MARKER_H

#include <QWidget>

class MainWindow;

class Markers : public QWidget
{
    Q_OBJECT
public:
    explicit Markers(QWidget *parent = nullptr);
private:
    MainWindow* mainWindow_;
signals:

public slots:
private slots:
    void paintEvent(QPaintEvent* event);
};

#endif // MARKER_H
