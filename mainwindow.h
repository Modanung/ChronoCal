/* ChronoCal
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "qdatetime.h"
#include <QMainWindow>
#include "qset.h"
#include "qhash.h"
#include "markers.h"

#define FILENAME_DATA "Data.xml"
#define MAX_DURATION 31622340

namespace Ui {
class MainWindow;
}

class Entry;
class Category;

struct Scope {

    QDate from_{};
    QDate to_{};
    QSet<Category*> categories_{};

};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

    void updateCategoryList();
    void pickEntryFromList(int row);

    const QVector<Entry*>& GetEntries() const { return entries_; }
    bool IsActiveEntry(Entry* entry) const { return entry == activeEntry_; }
    void setDialHue(int hue);
private slots:
    void on_actionQuit_triggered();
    void on_dial_valueChanged(int hue);
    void on_scopeComboBox_currentIndexChanged(int);
    void on_scopeSpinBox_valueChanged(int);
    void on_entryListWidget_currentRowChanged(int currentRow);
    void on_textEdit_textChanged();
    void on_titleEdit_editingFinished();
    void on_timeEdit_timeChanged(const QTime &time);
    void on_calendarWidget_selectionChanged();
    void on_addEntryButton_clicked();
    void on_removeEntryButton_clicked();
    void on_deleteEntryButton_clicked();

    void on_daysBox_editingFinished();
    void on_hoursBox_editingFinished();
    void on_minutesBox_editingFinished();

protected:
private:
    Markers markers_;

    unsigned sortId_;
    Ui::MainWindow* ui_;
    QString dataLocation_;
//    QVector<Category*> categories_;
    QVector<Entry*> entries_;
    Scope scope_;

    Category* activeCategory_;
    Entry* activeEntry_;

    QDate previouslySelectedDate_;

    void createTimer();
    void readEntriesFromFile();
    void writeDataFile();

    void updateTimeScope();
    void updateDuration();
    int maxProgress();
    void updateEntryList();
    void setActiveEntry(Entry* entry);
    void setActiveCategory(Category* category);

    void addEntry(const QDate &date);
    void removeActiveEntry();
    QPixmap CategoryIcon(Category* c);
    
private slots:
    void updateProgression();
    void on_daysBox_valueChanged(const QString &);
    void on_hoursBox_valueChanged(const QString &);
    void on_minutesBox_valueChanged(const QString &);
    void on_entryListWidget_doubleClicked(const QModelIndex &);
    void on_todayButton_clicked();
    void on_dateEdit_dateChanged(const QDate &date);
    void on_actionAbout_ChronoCal_triggered();
    void on_dateEdit_editingFinished();
    void on_timeEdit_editingFinished();
    void on_addCategoryPushButton_clicked();
    void on_categoryBox_currentIndexChanged(int index);
};


#endif // MAINWINDOW_H
