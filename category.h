/* ChronoCal
*/


#ifndef CATEGORY_H
#define CATEGORY_H

#include <QMouseEvent>
#include <QPushButton>

class Category: public QPushButton
{
    Q_OBJECT

public:
    static std::vector<Category*> categories_;
    explicit Category(QWidget *parent = nullptr, unsigned id = 0);
    ~Category();

    void setId(unsigned id) { id_ = id; }
    unsigned id() const { return id_; }
    bool validId(unsigned id);

    void setHue(int hue);
    int hue() const { return hue_; }

    void setName(const QString& name);
    const QString& name() const { return name_; }

    static bool byId(Category* lhs, Category* rhs)
    {
        return *lhs < *rhs;
    }
    bool operator < (const Category& other) const;
    bool operator > (const Category& other) const;

private slots:
    void handleClicked();
    void handleRightClicked();

protected slots:
    void paintEvent(QPaintEvent* event) override;
    void mousePressEvent(QMouseEvent *e) override;

private:
    unsigned id_;
    int hue_;
    QString name_;
    unsigned row_;
    void updateCategoryList();

signals:
    void rightClicked();
};

#endif // CATEGORY_H
