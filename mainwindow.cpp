/* ChronoCal
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "entry.h"
#include "category.h"
#include "markers.h"

#include <QMouseEvent>
#include <QTimer>
#include <QPainter>
#include <QMessageBox>
#include <QXmlStreamWriter>
#include <QStandardPaths>
#include <QDir>

#include <QDebug>
#include <assert.h>


MainWindow::MainWindow(QWidget *parent): QMainWindow(parent),
    markers_{ this },
    sortId_{},
    ui_{ new Ui::MainWindow },
    dataLocation_{ QStandardPaths::writableLocation(QStandardPaths::StandardLocation::AppLocalDataLocation) + '/' },
    entries_{},
    scope_{},
    activeCategory_{ nullptr },
    activeEntry_{}
{
    ui_->setupUi(this);
    //Trigger colour update for dial
    on_dial_valueChanged(ui_->dial->value());

    previouslySelectedDate_ = ui_->calendarWidget->selectedDate();
    setActiveEntry(nullptr);

    readEntriesFromFile();
    updateTimeScope();

    createTimer();

//m->setBaseSize(QSize(1, 1));
    markers_.setParent(ui_->calendarWidget);
}
MainWindow::~MainWindow()
{
    writeDataFile();
    delete ui_;
}
void MainWindow::createTimer()
{
    QTimer* const timer{ new QTimer(this) };
    connect(timer, SIGNAL(timeout()),this, SLOT(updateProgression()));
    timer->setInterval(1000);
    timer->start();
}

void MainWindow::readEntriesFromFile()
{
    QString filename = dataLocation_ + FILENAME_DATA;
    QFile file(filename);
    if (file.open(QIODevice::ReadOnly)) {

        QXmlStreamReader stream(&file);

        stream.readNext();
        while (!stream.atEnd() && !stream.hasError()) {

            QXmlStreamReader::TokenType token{ stream.readNext() };

            if(token == QXmlStreamReader::StartDocument) {
                    continue;
            }
            if(token == QXmlStreamReader::StartElement) {


                if (stream.name() == "category") {

                    Category* category{ nullptr };

                    for (const QXmlStreamAttribute &a : stream.attributes()) {

                        if (a.name() == "id") {
                            category = { new Category(this, a.value().toUInt()) };
                            ui_->categoryRow->addWidget(category);
                            break;
                        }
                    }

                    if (category) {

                        for (const QXmlStreamAttribute &a : stream.attributes()) {

                            if (a.name() == "hue") {
                                category->setHue(a.value().toInt());
                            }
                        }
                        category->setName(stream.readElementText());
                    }

                } else if (stream.name() == "entry") {

                    Entry* entry{ new Entry() };
                    entry->setSortId(sortId_++);

                    for (const QXmlStreamAttribute &a : stream.attributes()) {

                        if (a.name() == "title") {

                            entry->setTitle(a.value().toString());

                        } else if (a.name() == "start") {

                            QDateTime start{ QDateTime::fromMSecsSinceEpoch(a.value().toULongLong()) };
                            entry->setDate(start.date());
                            entry->setTime(start.time());

                        } else if (a.name() == "duration") {

                            entry->setDuration(a.value().toLong());
                        } else if (a.name() == "category") {

                            for (Category* c: Category::categories_)
                                if (c->id() == a.value().toInt())
                                    entry->setCategory(c);
                        }
                    }
                    entry->setBody(stream.readElementText());

                    entries_.append(entry);
                }
            }
            if (token == QXmlStreamReader::EndDocument) {

                break;
            }

        }
        if(stream.hasError()) {
                QMessageBox::critical(this,
                "Data.xml Parse Error", stream.errorString(),
                QMessageBox::Ok);
        }
    }
}

void MainWindow::writeDataFile()
{
    QString filename{ dataLocation_ + FILENAME_DATA };
    QFile file(filename);

    if (!QDir(dataLocation_).exists())
        QDir().mkdir(dataLocation_);

    if (file.open(QIODevice::WriteOnly)) {

        QXmlStreamWriter stream(&file);
        stream.setAutoFormatting(true);
        stream.writeStartDocument();

        stream.writeStartElement("data");
        stream.writeStartElement("categories");

        for (Category* c : Category::categories_) {

            stream.writeStartElement("category");

            stream.writeAttribute("id", QString::number(c->id()));
            stream.writeAttribute("hue", QString::number(c->hue()));
            stream.writeCharacters(c->name());

            stream.writeEndElement(); //category
        }

        stream.writeEndElement(); //categories

        stream.writeStartElement("entries");
        for (Entry* e : entries_) {

            stream.writeStartElement("entry");

            stream.writeAttribute("title", e->title());
            stream.writeAttribute("start", QString::number(e->dateTime().toMSecsSinceEpoch()));
            stream.writeAttribute("duration", QString::number(e->duration()));
            stream.writeAttribute("category", QString::number(e->category() ? e->category()->id() : 0));
            stream.writeCharacters(e->body());

            stream.writeEndElement(); //entry
        }

        stream.writeEndElement(); //entries
        stream.writeEndElement(); //data
        stream.writeEndDocument();
    }
}

void MainWindow::addEntry(const QDate &date)
{
    QTime currentTime{ QTime::currentTime().hour(), QTime::currentTime().minute() };

    Entry* entry{ new Entry(QDateTime(date, QDate::currentDate() == date ? currentTime : QTime(12, 0))) };
    entry->setSortId(sortId_++);

    entries_.append(entry);
    setActiveEntry(entry);
    updateEntryList();
}
void MainWindow::removeActiveEntry()
{
    entries_.removeOne(activeEntry_);
    delete activeEntry_;
    setActiveEntry(nullptr);
    updateEntryList();
}
void MainWindow::updateTimeScope()
{
    QDate date{ ui_->calendarWidget->selectedDate() };

    bool forward{ ui_->scopeComboBox->currentIndex() == 0 };
    int days{ ui_->scopeSpinBox->value() };

    scope_.from_ = date;
    scope_.to_ = scope_.from_.addDays(forward ? days : -days);

    updateEntryList();
}

void MainWindow::updateEntryList()
{
    QListWidget* entryList{ ui_->entryListWidget };
    entryList->clear();

    for (Entry* entry : entries_) {

        entry->setRow(-1);
    }
    qSort(entries_.begin(), entries_.end(), Entry::toAssending);

    int id{ 0 };
    for (Entry* entry : entries_) {

        if (entry->isWithinScope(scope_)) {

            QTime entryTime{ entry->time() };
            QString dateString{ entry->date().toString() };
            QString timeString{ QString::number(entryTime.hour()) + ':'};
            if (entryTime.minute() < 10)
                timeString += '0';
            timeString.append(QString::number(entryTime.minute()));

            entry->setRow(id);
            QListWidgetItem* item{ new QListWidgetItem };
            item->setText(dateString + ' ' + timeString + " - " + entry->title());

            QPixmap iconPixmap{ 7, 23 };
            iconPixmap.fill(Qt::transparent);
            QPainter iconPainter{ &iconPixmap };
            iconPainter.setRenderHint(QPainter::Antialiasing);
            int hue{ entry->category() ? entry->category()->hue() : 55 };
            bool saturated{ entry->category() != nullptr };
            QColor color{ QColor::fromHsl(hue, 255 * saturated, 128, 255) };

            iconPainter.setBrush(color);
            iconPainter.setPen(QColor::fromHsl(hue, 255 * saturated, 64, 255));
            iconPainter.drawRect(2, 1,
                                 3, iconPixmap.height() - 1);
            iconPainter.drawRect(1, 2,
                                 5, iconPixmap.height() - 3);

            iconPainter.setBrush(QColor::fromHsl(hue, 255 * saturated, 223, 255));
            iconPainter.setPen(QColor::fromHsl(hue, 255 * saturated, 160, 255));

            iconPainter.drawEllipse(2, 3, 2, iconPixmap.height() - 6);

            QIcon itemIcon(iconPixmap);
            item->setIcon(itemIcon);
            entryList->addItem(item);

            if (entry == activeEntry_)
                entryList->setCurrentRow(id, QItemSelectionModel::SelectionFlag::Select);

            ++id;
        }
    }
    if (activeEntry_ && !activeEntry_->isListed())
        entryList->setCurrentRow(-1);
}

void MainWindow::pickEntryFromList(int row)
{
    for (Entry* entry : entries_) {

        if (entry->isListed() && (entry->row() == row)) {

            setActiveEntry(entry);
            return;
        }
    }
}
void MainWindow::setActiveEntry(Entry* entry)
{
    if (activeEntry_ == entry && entry != nullptr)
        return;

    activeEntry_ = entry;

    if (activeEntry_) {


        ui_->categoryLabel->setEnabled(true);
        ui_->categoryBox->setEnabled(true);
        setActiveCategory(activeEntry_->category());

        ui_->titleLabel->setEnabled(true);
        ui_->titleEdit->setEnabled(true);
        ui_->titleEdit->setText(activeEntry_->title());

        ui_->dateLabel->setEnabled(true);
        ui_->dateEdit->setEnabled(true);
        ui_->dateEdit->setDate(activeEntry_->date());

        ui_->timeLabel->setEnabled(true);
        ui_->timeEdit->setEnabled(true);
        ui_->timeEdit->setTime(activeEntry_->time());

        qint64 seconds{ activeEntry_->duration() };
        int days{};
        int hours{};
        int minutes{};
        while (seconds >= 24 * 60 * 60 && days < ui_->daysBox->maximum()) {
            seconds -=    24 * 60 * 60;
            ++days;
        }
        while (seconds >= 60 * 60 && hours < 23) {
            seconds -=    60 * 60;
            ++hours;
        }
        while (seconds >= 60 && minutes < 59) {
            seconds -= 60;
            ++minutes;
        }

        ui_->durationLabel->setEnabled(true);
        ui_->daysBox->setEnabled(true);
        ui_->daysBox->setValue(days);
        ui_->hoursBox->setEnabled(true);
        ui_->hoursBox->setValue(hours);
        ui_->minutesBox->setEnabled(true);
        ui_->minutesBox->setValue(minutes);

        ui_->progressBar->setMaximum(std::max(1, static_cast<int>(activeEntry_->duration())));
        updateProgression();
        ui_->progressBar->setEnabled(true);

        ui_->textEdit->setEnabled(true);
        ui_->textEdit->setText(activeEntry_->body());

    } else {


        ui_->categoryLabel->setEnabled(false);
        ui_->categoryBox->setEnabled(false);
        setActiveCategory(nullptr);

        ui_->titleLabel->setEnabled(false);
        ui_->titleEdit->clear();
        ui_->titleEdit->setEnabled(false);

        ui_->dateLabel->setEnabled(false);
        ui_->dateEdit->setDate(ui_->calendarWidget->selectedDate());
        ui_->dateEdit->setEnabled(false);

        ui_->timeLabel->setEnabled(false);
        ui_->timeEdit->setTime(QTime(12, 0));
        ui_->timeEdit->setEnabled(false);

        ui_->durationLabel->setEnabled(false);
        ui_->daysBox->setEnabled(false);
        ui_->hoursBox->setEnabled(false);
        ui_->minutesBox->setEnabled(false);

        ui_->progressBar->setEnabled(false);
        ui_->progressBar->setValue(0);

        ui_->textEdit->clear();
        ui_->textEdit->setEnabled(false);
    }

    markers_.repaint();
}

void MainWindow::updateCategoryList()
{
    if (!ui_)
        return;


    int index{ ui_->categoryBox->currentIndex() };
    ui_->categoryBox->clear();
    ui_->categoryBox->addItem("None");

    for (unsigned i{ 0 }; i < Category::categories_.size(); ++i) {

        Category* c{ Category::categories_[i] };
        ui_->categoryBox->addItem(QIcon(CategoryIcon(c)), c->name());
    }
    ui_->categoryBox->setCurrentIndex(index);
}
QPixmap MainWindow::CategoryIcon(Category* c)
{
    QPixmap iconPixmap{ 16, 16 };
    iconPixmap.fill(Qt::transparent);
    QPainter iconPainter{ &iconPixmap };
    iconPainter.setRenderHint(QPainter::Antialiasing);
    int hue{ c->hue() };
    QColor color{ QColor::fromHsl(hue, 255, 128) };

    iconPainter.setBrush(color);
    iconPainter.setPen(QColor::fromHsl(hue, 255, 64));

    iconPainter.drawRoundedRect(2, 2, iconPixmap.width() - 2, iconPixmap.height() - 2, 4, 4);

    return iconPixmap;
}
void MainWindow::setActiveCategory(Category* category)
{
    activeCategory_ = category;
    if (activeCategory_ == nullptr)
        ui_->categoryBox->setCurrentIndex(0);
    else
        ui_->categoryBox->setCurrentIndex(activeCategory_->id());
}
void MainWindow::updateDuration()
{
    if (activeEntry_) {

        activeEntry_->setDuration(std::min(MAX_DURATION, ((24 * ui_->daysBox->value() + ui_->hoursBox->value()) * 60 + ui_->minutesBox->value()) * 60));
        ui_->progressBar->setMaximum(maxProgress());
        markers_.repaint();
        updateProgression();

        updateEntryList();
    }
}
void MainWindow::updateProgression()
{
    if (activeEntry_)
        ui_->progressBar->setValue(std::max(0, std::min(static_cast<int>(
                                                        activeEntry_->dateTime().secsTo(QDateTime::currentDateTime())),
                                                        maxProgress())));
}
int MainWindow::maxProgress()
{
    return std::max(1, static_cast<int>(activeEntry_->duration()));
}

void MainWindow::on_calendarWidget_selectionChanged()
{
    updateTimeScope();

    if (!activeEntry_) {

        ui_->dateEdit->setDate(ui_->calendarWidget->selectedDate());
    }

}
void MainWindow::on_scopeComboBox_currentIndexChanged(int)
{
    updateTimeScope();
}
void MainWindow::on_scopeSpinBox_valueChanged(int)
{
    updateTimeScope();
}


void MainWindow::on_entryListWidget_currentRowChanged(int currentRow)
{

    if (ui_->entryListWidget->count()) {

        pickEntryFromList(currentRow);
    }
}

void MainWindow::on_textEdit_textChanged()
{
    if (activeEntry_)
        activeEntry_->setBody(ui_->textEdit->toPlainText());
}

void MainWindow::on_titleEdit_editingFinished()
{
    if (activeEntry_) {

        activeEntry_->setTitle(ui_->titleEdit->text());
        updateEntryList();
    }

}

void MainWindow::on_dateEdit_dateChanged(const QDate &date)
{

}

void MainWindow::on_dateEdit_editingFinished()
{
    const QDate& date{ ui_->dateEdit->date() };
    if (activeEntry_) {

        activeEntry_->setDate(date);
        updateEntryList();
        markers_.repaint();
    }
}

void MainWindow::on_timeEdit_timeChanged(const QTime &time)
{
    if (activeEntry_) {

        activeEntry_->setTime(time);
        updateEntryList();
    }
}

void MainWindow::on_addEntryButton_clicked()
{
    addEntry(ui_->calendarWidget->selectedDate());
}
void MainWindow::on_removeEntryButton_clicked()
{
    if (activeEntry_ && activeEntry_->isListed()) {
        int row { ui_->entryListWidget->currentRow() };

        removeActiveEntry();

        row = std::min(ui_->entryListWidget->count() - 1, row);
        ui_->entryListWidget->setCurrentRow(row);
    }
}
void MainWindow::on_deleteEntryButton_clicked()
{
    if (activeEntry_)
        removeActiveEntry();
}

void MainWindow::on_daysBox_editingFinished()
{
    updateDuration();
}
void MainWindow::on_hoursBox_editingFinished()
{
    updateDuration();
}
void MainWindow::on_minutesBox_editingFinished()
{
    updateDuration();
}

void MainWindow::on_dial_valueChanged(int hue)
{
    ui_->dial->setPalette(QPalette(QColor::fromHsv(hue, 255, 223)));

    for (Category* c : Category::categories_) {

        if (c->isChecked()) {
            c->setHue(hue);
            c->repaint();
        }
    }

    markers_.repaint();
    updateEntryList();
    updateCategoryList();
}

void MainWindow::setDialHue(int hue)
{
    ui_->dial->setValue(hue);
    on_dial_valueChanged(hue);
}

void MainWindow::on_daysBox_valueChanged(const QString &)
{
//    updateDuration();
}
void MainWindow::on_hoursBox_valueChanged(const QString &)
{
//    updateDuration();
}
void MainWindow::on_minutesBox_valueChanged(const QString &)
{
//    updateDuration();
}

void MainWindow::on_actionQuit_triggered()
{
    close();
}

void MainWindow::on_entryListWidget_doubleClicked(const QModelIndex &)
{
    ui_->calendarWidget->setSelectedDate(activeEntry_->date());
}

void MainWindow::on_todayButton_clicked()
{
    ui_->calendarWidget->setSelectedDate(QDate::currentDate());
}

void MainWindow::on_actionAbout_ChronoCal_triggered()
{
    QString aboutText{ QString("<p><b>") +  "ChronoCal" + QString(" ")
                + qApp->applicationVersion() + QString(" </b></p>")
                + tr("<p>Copyright &copy; 2018 ChronoCal</b>"
                     "<p>You may use and redistribute this software under the terms "
                     "of the<br><a href=\"http://www.gnu.org/licenses/gpl.html\">"
                     "GNU General Public License Version 3</a>.</p>") };

    QMessageBox::about(this, tr("About %1").arg("ChronoCal"), aboutText);
}

void MainWindow::on_timeEdit_editingFinished()
{
    markers_.repaint();
}

void MainWindow::on_addCategoryPushButton_clicked()
{
    ui_->categoryRow->addWidget(new Category(this));
}

void MainWindow::on_categoryBox_currentIndexChanged(int index)
{
    if (activeEntry_) {

        activeEntry_->setCategory(index == 0 ? nullptr : Category::categories_[index - 1]);
        markers_.repaint();
        updateEntryList();
    }
}
