/* ChronoCal
*/

#include "category.h"
#include "mainwindow.h"
#include <QPainter>
#include <QDialog>
#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QLineEdit>

std::vector<Category*> Category::categories_{};

Category::Category(QWidget* parent, unsigned id) : QPushButton(parent),
    id_{ id },
    name_{},
    hue_{ random() % 256 }
{
    setCheckable(true);
    setAutoFillBackground(true);

    while (!validId(id_))
        ++id_;

    setName(QString::number(id_));

    categories_.push_back(this);
    qSort(categories_.begin(), categories_.end(), Category::byId);

    updateCategoryList();

    connect(this, &QPushButton::clicked, [this](){ handleClicked(); });
    connect(this, &Category::rightClicked, [this](){ handleRightClicked(); });
}

Category::~Category()
{
    auto it{ std::find(categories_.begin(), categories_.end(), this) };
    if (it != categories_.end()) {

        std::swap(*it, categories_.back());
        categories_.pop_back();
    }

    updateCategoryList();
}

void Category::setName(const QString& name)
{
    if (name == name_)
        return;

    name_ = name;
    setText(name_);

    updateCategoryList();
}

bool Category::validId(unsigned id)
{
    if (id == 0)
        return false;

    for (Category* c: categories_) {

        if (c->id_ == id) {

            return false;
        }
    }
    return true;
}

void Category::handleClicked()
{
    for (Category* c : categories_) {

        if (this != c)
            c->setChecked(false);
    }

    if (!isChecked())
        return;

    MainWindow* mainWindow{ qobject_cast<MainWindow*>(QPushButton::parent()->parent()) };
    if (mainWindow)
        mainWindow->setDialHue(hue());
}
void Category::handleRightClicked()
{
    QDialog* renameBox{ new QDialog(this) };
    renameBox->setWindowTitle("Rename Category");
    renameBox->setModal(true);
    renameBox->setMinimumWidth(320);

    QVBoxLayout* renameLayout{ new QVBoxLayout() };

    QLineEdit* nameEdit{ new QLineEdit() };
    nameEdit->setText(name_);
    renameLayout->addWidget( nameEdit );

    QDialogButtonBox* buttons{ new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel) };
    renameLayout->addWidget(buttons);

    renameBox->setLayout(renameLayout);
    connect(buttons, SIGNAL(accepted()), renameBox, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), renameBox, SLOT(reject()));

    if (!renameBox->exec())
        return;
    else
        setName(nameEdit->text());
}

void Category::setHue(int hue)
{
    if (hue == hue_)
        return;

    hue_ = hue;
    updateCategoryList();
}
void Category::paintEvent(QPaintEvent *event)
{
    QPushButton::paintEvent(event);

    QPainter p{ this };

    p.setRenderHint(QPainter::HighQualityAntialiasing);
    p.setBrush(Qt::transparent);
    p.setPen(QPen(QColor::fromHsl(hue_, 160, 128), 4.0));
    int border{ 2 };
    p.drawRoundedRect(QRect(border, border, width() - border * 2, height() - border * 2), border, border);
}

void Category::mousePressEvent(QMouseEvent *e)
{
    if(e->button() == Qt::RightButton)
        emit rightClicked();
}

bool Category::operator < (const Category& other) const
{
    return id() < other.id();
}
bool Category::operator > (const Category& other) const
{
    return id() > other.id();
}

void Category::updateCategoryList()
{
    MainWindow* mainWindow{ qobject_cast<MainWindow*>(QPushButton::parent()) };
    if (mainWindow)
        mainWindow->updateCategoryList();
}
