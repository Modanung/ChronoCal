/* ChronoCal
*/

#include "entry.h"
#include "category.h"
#include "mainwindow.h"

Entry::Entry():Entry(QDateTime::currentDateTime()) {}

Entry::Entry(QDateTime dateTime):
    row_{-1},
    category_{},
    title_{"Untitled"},
    dateTime_{dateTime},
    duration_{},
    body_{}
{

}

bool Entry::isWithinScope(const Scope& scope) const
{
    if (!scope.categories_.size() || scope.categories_.contains(category_)) {

        if (scope.to_ > scope.from_) {

            if (date() >= scope.from_ && date() <= scope.to_ ) {

                return true;

            } else if (duration_) {

                if (std::min(scope.to_, dateTime_.addSecs(duration_).date()) >=
                    std::max(scope.from_, date()))

                    return true;
            }

        } else {

            if (date() <= scope.from_ && date() >= scope.to_ ) {

                return true;

            } else if (duration_) {

                if ((std::min(scope.from_, dateTime_.addSecs(duration_).date()) >=
                     std::max(scope.to_, date())))

                    return true;
            }
        }
    }
    return false;
}

bool Entry::operator < (const Entry& other) const
{
    if (dateTime() == other.dateTime())
        if (duration() == other.duration())
            if (title() == other.title())

                return sortId() < other.sortId();
            else
                return  title() < other.title();
        else
            return duration() > other.duration();
    else
        return dateTime() < other.dateTime();

}

bool Entry::operator > (const Entry& other) const
{
    if (dateTime() == other.dateTime())
        if (duration() == other.duration())
            if (title() == other.title())

                return sortId() > other.sortId();
            else
                return  title() > other.title();
        else
            return duration() < other.duration();
    else
        return dateTime() > other.dateTime();

}
