QT       += core gui
QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = chronocal
TEMPLATE = app


SOURCES += \
    main.cpp\
    mainwindow.cpp \
    entry.cpp \
    category.cpp \
    markers.cpp

HEADERS += \
    mainwindow.h \
    entry.h \
    category.h \
    markers.h

FORMS += \
    mainwindow.ui

RESOURCES += \
    resources.qrc

unix
{
    isEmpty(DATADIR) DATADIR = $$(DESTDIR)$$(XDG_DATA_HOME)
    isEmpty(DATADIR) DATADIR = $$(DESTDIR)$$(HOME)/.local/share

    target.path = $$(DESTDIR)/usr/bin/
    INSTALLS += target

    icon.path = $$DATADIR/icons/
    icon.files = chronocal.svg
    INSTALLS += icon

    desktop.path = $$DATADIR/applications/
    desktop.files = chronocal.desktop
    INSTALLS += desktop
}
