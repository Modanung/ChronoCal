/* ChronoCal
*/

#include "markers.h"
#include "mainwindow.h"
#include "entry.h"
#include "category.h"

#include <QApplication>
#include <QPainter>
#include <QCalendarWidget>
#include <QDebug>
#include <QTableView>

Markers::Markers(QWidget *parent) : QWidget(parent),
    mainWindow_{static_cast<MainWindow*>(parent)}
{
    setAttribute(Qt::WA_TransparentForMouseEvents);
}

void Markers::paintEvent(QPaintEvent *event)
{
    QCalendarWidget* calendar{ qobject_cast<QCalendarWidget*>(parent()) };
    QTableView* view{ calendar->findChild<QTableView*>("qt_calendar_calendarview") };

    QSize cornerSize{ view->columnWidth(0), view->rowHeight(0) };
    setGeometry(QRect(view->pos() + QPoint(cornerSize.width(), cornerSize.height()), view->size() - cornerSize));

    QSizeF cellSize(width() / 7.0, height() / 6.0);
    QSizeF markerSize{ cellSize.height() / 12, cellSize.height() / 12};

    QPainter p(this);

    p.setRenderHint(QPainter::HighQualityAntialiasing);

    QDate firstDayOfMonthShown{ calendar->yearShown(), calendar->monthShown(), 1};
    QDate firstDayShown{ firstDayOfMonthShown.addDays(-firstDayOfMonthShown.dayOfWeek()) };
    QDate lastDayShown{ firstDayShown.addDays(41) };

    int firstWeekShown{ firstDayShown.weekNumber() };
    if (calendar->firstDayOfWeek() == 7)
        firstWeekShown += 1;

    Scope scope{};
    scope.from_ = firstDayShown;
    scope.to_ = lastDayShown;

    for (Entry* e : mainWindow_->GetEntries()) {

        if (!e->isWithinScope(scope)) {

            continue;
        }

        int markerColumn = calendar->firstDayOfWeek() == 7 ? e->date().dayOfWeek() % 7
                                                           : e->date().dayOfWeek() - 1;

        QPointF markerPos{ e->time().msecsSinceStartOfDay() * cellSize.width() / 86400000.0 + cellSize.width() * markerColumn,
                    /*overlap * cellSize.height() / 5.0 + */cellSize.height() / 8.0 + cellSize.height() * (e->date().weekNumber() - firstWeekShown) };
        if (e->date().dayOfWeek() == 7)
            markerPos += QPointF(0.0, cellSize.height());

        Category* c{ e->category() };
        QColor markerColor(c ? QColor::fromHsl(c->hue(), 255, 128)
                             : QColor::fromHsl(0, 0, 192));

        //Draw line
        double penWidth{ markerSize.height() * 0.42 };
        p.setPen(QPen(markerColor, penWidth));
        QPointF lineStart( markerPos + QPointF(markerSize.width() + penWidth * 0.5, 0) );
        double lineLength{ std::max(0.0, (e->duration() * cellSize.width() / 86400.0) - penWidth * 0.25 - markerSize.width())};

        while (lineLength > 0.0) {

            p.drawLine(lineStart, lineStart + QPointF(lineLength, 0));
            lineLength = lineStart.x() + lineLength - width();
            lineStart = QPointF(0, lineStart.y() + cellSize.height());
        }

        //Draw bubble
        p.setPen(QPen(markerColor, markerSize.height() * 0.34f));
        markerColor.setAlpha(mainWindow_->IsActiveEntry(e) ? 255 : 64);
        p.setBrush(markerColor);
        p.drawEllipse(markerPos, markerSize.width(), markerSize.height());

        //Draw bubble specs
        p.setBrush(Qt::white);
        p.setPen(Qt::transparent);
        double specSize{ markerSize.height() * 0.25 };
        QPointF specOffset{ specSize * 1.42, specSize * 1.23 };
        p.drawEllipse(markerPos - specOffset, specSize, specSize);
        p.setBrush(QColor(255, 255, 255, 128));
        p.drawEllipse(markerPos + specOffset, specSize, specSize);
    }

//    p.drawRect(0,0,width(), height());
}


