/* ChronoCal
*/

#ifndef ENTRY_H
#define ENTRY_H

#include "qdatetime.h"

class Category;
struct Scope;

class Entry
{
public:
    Entry();
    Entry(QDateTime dateTime);

    void setRow(int id) { row_ = id; }
    void setSortId(unsigned sortId) { sortId_ = sortId; }
    void setDate(const QDate& date) { dateTime_.setDate(date); }
    void setTime(const QTime& time) { dateTime_.setTime(time); }
    void setDuration(const qint64 duration) { duration_ = duration; }
    void setTitle(const QString& title) { title_ = title; }
    void setBody(const QString& body) { body_ = body; }
    void setCategory(Category* category) { category_ = category; }

    bool isWithinScope(const Scope& scope) const;

    bool isListed() const { return row_ > -1; }
    int row() const { return row_; }
    unsigned sortId() const { return sortId_; }

    Category* category() const { return category_; }
    QString title() const { return title_; }

    QDateTime dateTime() const { return dateTime_; }
    QDate date() const { return dateTime_.date(); }
    QTime time() const { return dateTime_.time(); }
    qint64 duration() const { return duration_; }
    const QString& body() const { return body_; }

    static bool toAssending(Entry* lhs, Entry* rhs)
    {
        return *lhs < *rhs;
    }
    bool operator < (const Entry& other) const;
    bool operator > (const Entry& other) const;
private:
    int row_;
    unsigned sortId_;

    Category* category_;
    QString title_;

    QDateTime dateTime_;
    qint64 duration_;

    QString body_;
};

#endif // ENTRY_H
